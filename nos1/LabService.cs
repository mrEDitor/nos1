﻿using System;
using System.Numerics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Threading;

namespace nos1
{
    public class LabService : ServiceBase
    {
        private static readonly string DIR_PATH = "C:\\lab1";
        private static readonly string LOG_PATH = $"{DIR_PATH}\\log.txt";
        private static readonly string IN_PATH = $"{DIR_PATH}\\in.txt";
        private static readonly string OUT_PATH = $"{DIR_PATH}\\out.txt";
        private Timer _timer;

        public LabService()
        {
            ServiceName = "lab1";
            CanStop = true;
            CanPauseAndContinue = false;
        }

        protected override void OnStart(string[] args)
        {
            Directory.CreateDirectory(DIR_PATH);
            File.AppendAllText(LOG_PATH, $"{DateTime.Now} Starting lab1 service\n");
            _timer = new Timer(OnTimer, null, TimeSpan.Zero, TimeSpan.FromSeconds(1));
        }

        private void OnTimer(object state)
        {
            File.AppendAllText(LOG_PATH, $"{DateTime.Now} Enter OnTimer\n");
            try
            {
                File.WriteAllLines(OUT_PATH, File.ReadAllLines(IN_PATH).Where(IsEven));
            }
            catch (Exception e)
            {
                File.WriteAllText(OUT_PATH, e.ToString() + "\n");
            }
            File.AppendAllText(LOG_PATH, $"{DateTime.Now} Leave OnTimer\n");
        }

        private bool IsEven(string arg) =>
            string.IsNullOrWhiteSpace(arg) || (BigInteger.Parse(arg) % 2) == 0;

        protected override void OnStop()
        {
            File.AppendAllText(LOG_PATH, $"{DateTime.Now} Stopping lab1 service\n");
            _timer.Dispose();
        }
    }
}
