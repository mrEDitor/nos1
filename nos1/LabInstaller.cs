﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace nos1
{
    [RunInstaller(true)]
    public class LabInstaller : Installer
    {
        public LabInstaller()
        {
            var serviceInstaller = new ServiceInstaller
            {
                StartType = ServiceStartMode.Manual,
                ServiceName = "lab1",
                DisplayName = "lab1: even filter master pro"
            };
            var processInstaller = new ServiceProcessInstaller
            {
                Account = ServiceAccount.LocalSystem
            };
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
