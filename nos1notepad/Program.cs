﻿using System;
using System.IO;

namespace nos1notepad
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var output = File.CreateText("C:\\lab1\\in.txt"))
            {
                while (true)
                {
                    var keyPress = Console.ReadKey(true);
                    if (keyPress.Key == ConsoleKey.Escape)
                    {
                        output.WriteLine();
                        output.Flush();
                        return;
                    }
                    else if (keyPress.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine();
                        output.WriteLine();
                        output.Flush();
                    }
                    else if ('0' <= keyPress.KeyChar && keyPress.KeyChar <= '9')
                    {
                        Console.Write(keyPress.KeyChar);
                        output.Write(keyPress.KeyChar);
                    }
                    else
                    {
                        Console.Beep();
                    }
                }
            }
        }
    }
}
