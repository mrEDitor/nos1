﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceProcess;

namespace nos1
{
    public static class Program
    {
        private static readonly string INSTALL_UTIL =
            Path.Combine(RuntimeEnvironment.GetRuntimeDirectory(), "installutil.exe");

        private static readonly string THIS_EXE =
            new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath.Replace("\"", "\\\"");

        public static void Main(string[] args)
        {
            switch (args.FirstOrDefault())
            {
                case "install":
                    Install();
                    break;
                case "uninstall":
                    Uninstall();
                    break;
                case null:
                    Run();
                    break;
                default:
                    Console.WriteLine("pass install or uninstall command as argument");
                    break;
            }
        }

        private static void Install()
        {
            Console.WriteLine("installing...");
            Console.WriteLine($"{INSTALL_UTIL} \"{THIS_EXE}\"");
            Process.Start(INSTALL_UTIL, $"\"{THIS_EXE}\"");
        }

        private static void Uninstall()
        {
            Console.WriteLine("uninstalling...");
            Console.WriteLine($"{INSTALL_UTIL} {THIS_EXE} /u");
            Process.Start(INSTALL_UTIL, $"\"{THIS_EXE}\" /u");
        }

        private static void Run()
        {
            ServiceBase.Run(new ServiceBase[]
            {
                new LabService()
            });
        }
    }
}
